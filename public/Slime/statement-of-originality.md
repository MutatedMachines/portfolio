---
# for information on what this file is, see
# https://cs.anu.edu.au/courses/comp1720/resources/faq/#statement-of-originality

# for styling of references, see
# https://www.acm.org/publications/authors/reference-formatting

declaration: >-
  I declare that everything I have submitted in this assignment is entirely my
  own work except for the references listed below

# sign *your* name and uid here
name: Josh Carberry
uid: u4386672

# list numbered references below (replace the example with your own references) 
---
# References
- [1] The design of my slime was inspired by this meme where a frog takes blob form
https://www.reddit.com/r/aww/comments/o2bp8n/lemme_just_blob_for_u/
- [2] this page detailed a method i used to remove food from the food array https://stackoverflow.com/questions/5767325/how-can-i-remove-a-specific-item-from-an-array
- [3] I adapted a function (vector_length) of the book "2D GAME COLLISION DETECTION" to calcualte the distance the slime is from the food entity
2D GAME COLLISON Thomas Schwarzal 1st edition (22 November 2012) ASIN:B00ACJ088S ISBN-10:1479298123
https://www.amazon.com.au/2D-Game-Collision-Detection-Introduction/dp/1479298123
- [4] Examples of linking multiple javascript files used to link slime.js in my index.html
http://web.simmons.edu/~grabiner/comm244/weeknine/including-javascript.html
- [5] Cartasian to radian
https://stackoverflow.com/questions/1878907/how-can-i-find-the-difference-between-two-angles
- [6] radian to Cartasian (modified slightly to remain normalized)
https://www.codegrepper.com/code-examples/javascript/javascript+get+x%2Cy+point+on+circle


var divID = "cvn0"
//var div = document.getElementById(divID)

var cvn

var root

function setup() {
    var canvasDiv = document.getElementById(divID);
    var width = canvasDiv.offsetWidth;
    var height = canvasDiv.offsetHeight;
    var sketchCanvas = createCanvas(width,height);
    sketchCanvas.parent(divID);
    background('#f0f8')

    root = new Node("root")
    var chest = new Node("chest",root)
    var neck = new Node("neck",chest)
    var head = new Node("head",neck)
    var shoulderL = new Node("Left shoulder",chest)
    var elbowL = new Node ("Left elbow ",shoulderL)
    var handL = new Node("Left hand",elbowL)
    
    var shoulderR = new Node("Right shoulder",chest)
    var elbowR = new Node ("Right elbow",shoulderR)
    var handR = new Node("Right hand",elbowR)

    var pelvis = new Node("Pelvis",root)
    var Knee = new Node("Knee",pelvis)
    var foot = new Node("foot",Knee)
}


var mouseDown
function mouseClicked(){
    mouseDown = true
}

var mouseV
function draw(){
    
    background('#0fff')
    background('#f0f8')

//    ellipse(90, 90, 90, 90)

    mouseV = {x:mouseX,y:mouseY}
    text(""+mouseX,300,100)

 //   test_collision(5)

    root.draw(100,100,20,25)

    mouseDown = false
}
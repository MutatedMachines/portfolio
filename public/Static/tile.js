



class map_room extends Rectangle{

    aspect = {x:random(1,4),y:random(1,4)}

    constructor(x,y,w,h){
        super(x,y,w,h)
        this.size.x *= this.aspect.x
        this.size.y *= this.aspect.y
    }
}


var bool_flip = [0,1]


class map_index{
    x
    y
    
    w 
    h

    grid = []

    join ={
        left :boolean(random(bool_flip)),
        right :boolean(random(bool_flip)),
        up :boolean(random(bool_flip)),
        down :boolean(random(bool_flip)),
    }


    constructor(x,y,w,h){
        this.x = x
        this.y = y
        this.w = w
        this.h = h
//        this.join.down = y % 2 != 0 
//        this.join.right = x % 4 != 0 
    }

    Combine(other = new map_index(),iter =1){
        var r = new map_index(
            other.join.right  ? min(this.x,other.x) : other.x,
            other.y,

            ceil((this.join.right  ?  this.w + other.w : other.w) /iter),
            1//sthis.join.down  && other.join.up? this.h + other.h : this.h
        )


        this.w = this.join.right ? r.w :this.w
        this.x = this.join.right ? r.x :this.x


        r.join = other.join
        

        return r
    }


    draw(tileSize,hori = false){
        var pos = {x:this.x * tileSize.x,y:this.y * tileSize.y}
        var siz = {x:tileSize.x * this.w,y: tileSize.y * this.h}


        stroke('#000a')
        if(hori)
            line(pos.x,pos.y,pos.x + siz.x,pos.y)
        else
            line(pos.x,pos.y,pos.x,pos.y+siz.y)

    }
}

class map_map extends Rectangle{

    dim = new Vector2D()

    grid =[]
    grid2 = []
    combined = []

    mapgrid

    rooms = []

    constructor(w,h){
        super(0,0,width,height)

        this.dim.x = w
        this.dim.y = h

        for(var y = 0; y < h;y++){
            for(var x = 0; x < w;x++){
                var ind = (new map_index(x,y,1,1))
                if(x == w-1)
                    ind.join.right = false

                if(y == h-1)
                    ind.join.down = false

                this.grid.push(ind)
            }
        }



        var gridX = []
        var c =1
        this.grid.forEach((v,i,a) => {
            if(v.join.right == true )
                c++
            else{
                this.combined.push(new map_index(
                    v.x - c +1,
                    v.y,
                    c,
                    1
                ))
                c= 1
            }
        })

        c =1
        

        this.grid = this.grid.sort((a,b)=> {return a.x - b.x})
        var gridY = []

        this.grid.forEach((v,i,a) => {
            if(v.join.down == true )
                c++
            else{
                gridY.push(new map_index(
                    v.x ,
                    v.y - c  +1 ,
                    1,
                    c 
                ))
                c= 1
            }
        })

        


        this.grid = this.combined
        this.grid2 = gridY.sort((a,b)=> {return (a.x+ a.y*w)  - (b.x+ b.y*w)})

        /*

        for(var i =0, a =this.grid; i < a.length;i+=1 ){ 
            if(i < a.length-1){
                //if(a[1+1].join.right)
                a[i +1] = ( a[i].Combine(a[i+1],2) )                
            }
        }

        for(var i =0, a =this.grid; i < a.length;i+=1 ){ 
            if(i < a.length-1){
                //if(a[1+1].join.right)
                a[i +1] = ( a[i].Combine(a[i+1]) )                
            }
        }

        var unique = []


        this.grid.forEach((v,i,a) => {
            if(unique.find(p=> p.x == v.x  && p.y == v.y) ==null)
                unique.push(v)
            
        })
        */

        //this.grid = unique

        this.mapgrid = new tilegrid(this.grid2,w,h)

        
    //    this.combined = this.combined.map((v,i,a)=>{
    //        if(i != a.length)
    //        return a[i].Combine(a[i+1]) 
    //    })
    }



    draw(){
        fill('#888d')
        super.draw()



        var tileSize = {x: width/this.dim.x,y: height/this.dim.y}
        this.grid.forEach((v,i,a)=>{        
            fill('#0f08')
            v.draw(tileSize)
        })

        this.grid2.forEach((v,i,a)=>{
            fill('#f0f8')
            v.draw(tileSize,true)    
        })


            

//        this.mapgrid.draw(50,50)
    }
}




class tilegrid{
    
    grid = [[1,1,1,1,1],
            [1,0,0,0,1],
            [1,0,0,0,1],
            [1,0,0,0,1],
            [1,1,1,1,1]]
    
        dim = new Vector2D()
        

    tp = {
        x:8,
        y:8
    }

    constructor(grid,w,h){
        this.grid = grid

        this.dim.x =w
        this.dim.y =h
       // for(var i =0; i< this.dim.y;i++){
       //     if(this.grid[i].length > this.dim.x)
       //         this.dim.x = this.grid[i].length
       // }



    }


    draw(posX,posY){

        for(var y =0; y < this.dim.y;y++)
        for(var x =0; x < this.dim.x;x++){


            switch(this.grid[x + y* this.dim.x]){
                case 0 : fill('#333f') 
                break;
                case 1 : fill('#f00f')
                break
                case 2 : fill('#ff0f')
                break
                
                case 3 : fill('#f0ff')
                break
                
                case 4 : fill('#00ff')
                break
                default:
                    noFill()
                    break;
            }
 //           fill("#ff0f")

            rect(posX + x*this.tp.x, posY + y*this.tp.y, this.tp.x, this.tp.y)
        }
    }
}



function* nodeIndexer(index){
    while(true){
        yield index++
    }
}



var held;
var released;
var logStrings = []

class Node{
    name = "unset"
    parent
    children = []
    rank =-1
    gen =-1

    hPos = {x:0,y:0}

    hide =false
    shape
    nameShape

    selected = false

    constructor(name = "unset",parent=null){
        this.name = name
        if(parent != null)
    //        if(typeof(parent) === typeof(Node))
                parent.addChild(this)


                
       this.shape = Rectangle.create(10,10,10,10)
        this.nameShape = Rectangle.create(10,10,10,10)
    }

    addChild(child){
        this.children.push(child)
        child.parent = this

        this.getRoot().rankDecendants()
        this.getRoot().setGeneration()
        this.hPos.x = this.gen
        this.hPos.y = this.rank
    }

    setParent(Parent){
        this.parent.children.splice(this.parent.children.indexOf(this), 1)
        Parent.addChild(this)
    }

    refresh(){
        this.getRoot().rankDecendants()
        this.getRoot().setGeneration()
        this.hPos.x = this.gen
        this.hPos.y = this.rank
    }


    get Decendents() { return this.countDecendants() -1}

    countDecendants(ind =0){
        this.children.forEach((v,i,a)=>{
            ind = v.countDecendants(ind)
        }
        )
        ind++
        return ind 
    }


    //hIndex 

    getRoot(){
        if(this.parent!=null)
            return this.parent.getRoot()
        else return this
    }

    setGeneration(ind = 0){
        this.gen = ind

        if(this.hide == false)
        ind++
        this.children.forEach((v,i,a)=>{
           v.setGeneration(ind) 
        })
    }

    rankDecendants(){
        var nodeIndex = nodeIndexer(0)
        this.setRank(nodeIndex)
    }
    setRank(ind= nodeIndexer(0)){
        this.rank = ind.next().value
        this.children.forEach((v,i,a)=>{
            if(this.hide == false)
            v.setRank( ind)
        })
    }




    offset
    draw(x,y,w,h){
        push()
       // translate(w/10, 0)
       
       this.hPos.x = this.gen +1
       this.hPos.y = this.rank

       this.offset = {x: x+ this.hPos.x * w,y:y+ this.hPos.y*h}

       this.shape.origin = {x:this.offset.x - w*1.5,y:this.offset.y - h/4}
       this.shape.size = {x:w *2.5 ,y:h}

        this.nameShape.origin = {x:this.offset.x +w,y:this.offset.y - h/4}
        this.nameShape.size = {x:w*10,y:h}

       var mouseV = new Vector2D(mouseX,mouseY)
        var hResult = point_rectangle_collide( mouseV,this.shape)

        var nameResult = point_rectangle_collide( mouseV,this.nameShape)



        if(hResult && mouseDown ){
            this.hide = !this.hide
            this.refresh()     
        }


        //hold


        if(nameResult && mouseIsPressed && held == null)
            held = this
        if(nameResult && mouseDown == false)
            released = this

        //release
        if(mouseIsPressed == false){
            if(held != null){
                console.log(held.name,released.name)

                if(released != null)
                    if(released != held)
                        held.setParent(released)
                held = null
            }
            held = null
        }


        textSize(h)

        


        push()
        this.nameShape.draw()

//        if(held == this)

        //hiararchy backdrop
        fill("#1113")

    
        var isLast = this.children.length ==0
        if(isLast == false)
        this.shape.draw()

        //select
        if(nameResult && mouseDown)
            this.selected = true
        if(this.selected){
            fill("#22c3")
            this.nameShape.draw()
        }

        //held
        if( held ==this){
            fill("#8886")
            this.nameShape.draw()
        }
        
        this.nameShape.draw()
        pop()
        text(this.name,this.offset.x + w*1.5,this.offset.y + h/2)

        this.children.forEach((v,i,a) => {
            if(this.hide == false)
            v.draw(x,y,w,h)
        })


        for(var n =0; n < this.children.length;n++){
            
            var child = this.children[n]


           // var offspringY = y+h + (lastOffspringRank -0.5) *h

            
           if(this.hide == false){
                line(this.offset.x,     child.offset.y+h/4,
                    this.offset.x + w*2,   child.offset.y +h/4)

            
                line(this.offset.x ,this.offset.y +h/4,this.offset.x,child.offset.y +h/4)
            }
        }

        //if(this.hide == false)
        

        pop()
        

 
    }
}



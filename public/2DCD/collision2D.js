

function overlapping(minA,maxA,minB,maxB){
    return minB <= maxA && minA <= maxB
}

function rectangles_collide(a = new Rectangle(),b= new Rectangle()){
    var aLeft = a.origin.x
    var aRight = aLeft + a.size.x
    var bLeft = b.origin.x
    var bRight = bLeft + b.size.x
    
    var aBottom = a.origin.y
    var aTop = aBottom + a.size.y

    
    var bBottom = b.origin.y
    var bTop = bBottom + b.size.y

    return overlapping(aLeft,aRight,bLeft,bRight) && overlapping(aBottom,aTop,bBottom,bTop)
}

function circles_collide(a = new Circle(),b = new Circle()){
    var radiusSum = a.radius + b.radius
    var distance = subtract_vector(a.center,b.center)
    return vector_length(distance) <= radiusSum
}

function points_collide(a=new Vector2D(),b = new Vector2D()){
    return equal_floats(a.x,b.x) && equal_floats(a.y,b.y)
}

function rotate_vector_90(v=new Vector2D()){
    return new Vector2D(-v.y,v.x)
}

function parallel_vectors(a=new Vector2D(),b=new Vector2D()){
    var na = rotate_vector_90(a)
    return equal_floats(0,dot_product(na,b))
}

function equal_vectors(a= new Vector2D(),b = new Vector2D()){
    return equal_floats(a.x-b.x,0) && equal_floats(a.y-b.y,0)
}

function equivalent_lines(a= new Line(),b = new Line){
    if(!parallel_vectors(a.dir,b.dir))
        return false
    var d = subtract_vector(a.base,b.base)
    return parallel_vectors(d,a.dir)
}

function lines_collide(a = new Line(),b= new Line()){
    if(parallel_vectors(a.dir,b.dir))
        return equivalent_lines(a,b)
    else
        return true
}

function on_one_side(axis = new Line(),s= new Segment()){
    var d1 = subtract_vector(s.point1,axis.base)
    var d2 = subtract_vector(s.point2,axis.base)
    var n = rotate_vector_90(axis.dir)
    return dot_product(n,d1) * dot_product(n,d2) > 0
}

function sort_range(r = new sRange()){
    if(r.minimum > r.maximum){
        return new sRange(r.maximum,r.minimum)
    }else
        return  new sRange(r.minimum,r.maximum)
}

function project_segement(s = new Segment(),onto= new Vector2D()){
    var ontoUnit = unit_vector(onto)
    var r = new sRange()
    r.minimum = dot_product(ontoUnit,s.point1)
    r.maximum = dot_product(ontoUnit,s.point2)
    r = sort_range(r)
    return r
}

function overlapping_ranges(a = new sRange(),b = new sRange()){
    return overlapping(a.minimum,a.maximum,b.minimum,b.maximum)
}

function segments_collide(a = new Segment(),b = new Segment){
    var axisA = new Line(a.point1,subtract_vector(a.point2,a.point1))
    if(on_one_side(axisA,b))
        return false
    var axisB = new Line(b.point1,subtract_vector(b.point2,b.point1))
    if(on_one_side(axisB,a))
        return false
    if(parallel_vectors(axisA.dir,axisB.dir)){
        var rangeA = project_segement(a,axisA.dir)
        var rangeB = project_segement(b,axisA.dir)

        return overlapping_ranges(rangeA,rangeB)
    }
    else return true
}

function range_hull(a = new sRange(),b = new sRange()){
    var hull = new sRange()
    hull.minimum = a.minimum < b.minimum ? a.minimum : b.minimum
    hull.maximum = a.maximum > b.maximum ? a.maximum : b.maximum
    return hull
}

function oriented_rectangle_edge(r = new OrientedRectangle(),nr = 0){
    var edge = new Segment()
    var a = clone_vector(r.halfExtend)
    var b = clone_vector(r.halfExtend)
    switch(nr %4){
        case 0:
            a.x = -a.x
            break;
        case 1:
            b.y = -b.y
            break;
        case 2:
            a.y = -a.y
            b = negate_vector(b)
            break;
        default:
            a = negate_vector(a)
            b.x = -b.x
            break;
    }

    a = rotate_vector(a,r.rotation)
    a = add_vector(a,r.center)
    b = rotate_vector(b,r.rotation)
    b = add_vector(b,r.center)

    edge.point1 = a
    edge.point2 = b

    return edge;
}

function separating_axis_for_oriented_rectangle(axis = new Segment(),r = new OrientedRectangle()){
    var rEdge0 = oriented_rectangle_edge(r,0)
    var rEdge2 = oriented_rectangle_edge(r,2)

    var n = subtract_vector(axis.point1,axis.point2)

    var axisRange = project_segement(axis,n)
    var r0Range = project_segement(rEdge0,n)
    var r2Range = project_segement(rEdge2,n)
    var rProjection = range_hull(r0Range,r2Range)

    return !overlapping_ranges(axisRange,rProjection)
}



function oriented_rectangles_collide(a = new OrientedRectangle(),b= new OrientedRectangle()){
    var edge = oriented_rectangle_edge(a,0)
    if(separating_axis_for_oriented_rectangle(edge,b))
        return false
    edge = oriented_rectangle_edge(a,1)
    if(separating_axis_for_oriented_rectangle(edge,b))
        return false 
    edge = oriented_rectangle_edge(b,0)
    if(separating_axis_for_oriented_rectangle(edge,a))
        return false 
        edge = oriented_rectangle_edge(b,1)
    return (!separating_axis_for_oriented_rectangle(edge,a))

}

function oriented_rectangles_collide2(a = new OrientedRectangle(),b= new OrientedRectangle()){
    //var hit = false

    for(var i =0; i < 4;i++){
        var edgeA = oriented_rectangle_edge(a,i)
        if(separating_axis_for_oriented_rectangle(edgeA,b))
            return false 
        var edgeB = oriented_rectangle_edge(b,i)
        if(separating_axis_for_oriented_rectangle(edgeB,a))
            return false 
    }
     
    return true
}

function circle_point_collide(c = new Circle(),p = new Vector2D()){
    var distance = subtract_vector(c.center,p)
    return vector_length(distance) <= c.radius
}

function circle_line_collide(c= new Circle(),l=new line()){
    var lc = subtract_vector(c.center,l.base)
    var p = project_vector(lc,l.dir)
    var nearest = add_vector(l.base,p) // TODO important to isolate for cliping
    return circle_point_collide(c,nearest)
}

function circle_segment_collide(c = new Circle(),s = new Segment()){
    if(circle_point_collide(c,s.point1))
        return true
    if(circle_point_collide(c,s.point2))
        return true
    
    var d = subtract_vector(s.point2,s.point1)
    var lc = subtract_vector(c.center,s.point1)
    var p = project_vector(lc,d)
    var nearest = add_vector(s.point1,p)

    return (circle_point_collide(c,nearest) && 
        vector_length(p) <= vector_length(d) &&
        0 <= dot_product(p,d))


}

function clamp_on_range(x,min,max){
    if(x < min)
        return min
    else if (max < x)
        return max
    else
        return x
}

function clamp_on_rectangle(p = new Vector2D(), r =new Rectangle()){
    clamp = new Vector2D();
    clamp.x = clamp_on_range(p.x,r.origin.x,r.origin.x + r.size.x)
    clamp.y = clamp_on_range(p.y,r.origin.y,r.origin.y + r.size.y)
    return clamp
}

function circle_rectangle_collide(c = new Circle(),r = new Rectangle()){
    var clamped = clamp_on_rectangle(c.center,r)
    return circle_point_collide(c,clamped)
}

function circle_oriented_rectangle_collide(c = new Circle(),r = new OrientedRectangle()){
    var lr = new Rectangle()
    lr.origin.x = 0;
    lr.origin.y = 0;
    lr.size = multiply_vector(r.halfExtend,2)
    var lc = new Circle({x:0,y:0},c.radius)
    var distance = subtract_vector(c.center,r.center)
    distance = rotate_vector(distance,-r.rotation)

    lc.center = add_vector(distance,r.halfExtend)
    return circle_rectangle_collide(lc,lr)
}

function point_rectangle_collide(p = new Vector2D(),r = new Rectangle()){
    return r.origin.x <= p.x &&
    r.origin.x + r.size.x >= p.x &&
    r.origin.y <= p.y &&
    r.origin.y + r.size.y >= p.y    
}

function line_rectangle_collide(l= new Line(),r = new Rectangle()){
    var n = rotate_vector_90(l.dir)
    var c1 = r.origin
    var c2 = add_vector(c1,r.size)
    var c3 = new Vector2D(c2.x,c1.y)
    var c4 = new Vector2D(c1.x,c2.y)
    c1 = subtract_vector(c1,l.base)
    c2 = subtract_vector(c2,l.base)
    c3 = subtract_vector(c3,l.base)
    c4 = subtract_vector(c4,l.base)

    var dp1 = dot_product(n,c1)
    var dp2 = dot_product(n,c2)
    var dp3 = dot_product(n,c3)
    var dp4 = dot_product(n,c4)
    return (dp1 * dp2 <=0) ||
    (dp2 * dp3 <=0) ||
    (dp3 * dp4 <= 0);
}

function rectangle_segment_collide(r = new Rectangle(),s = new Segment()){
    var sLine = new Line()
    sLine.base = s.point1
    sLine.dir = subtract_vector(s.point2,s.point1)
    if(!line_rectangle_collide(sLine,r))
        return false
    
    var rRange = new sRange(r.origin.x,r.origin.x + r.size.x)
    var ssRange = new sRange(s.point1.x,s.point2.x)
    ssRange = sort_range(ssRange)
    if(!overlapping_ranges(rRange,ssRange))
        return false
    
    rRange = new sRange(r.origin.y,r.origin.y + r.size.y)
    ssRange = new sRange(s.point1.y,s.point2.y)
    ssRange = sort_range(ssRange)
    
    return overlapping_ranges(rRange,ssRange)
}

function rectangle_corner(r= new Rectangle(),nr){
    var corner = new Vector2D(r.origin.x,r.origin.y)
    switch(nr % 4){
        case 0:
            corner.x += r.size.x
            break;
        case 1:
            corner = add_vector(r.origin,r.size)
            break;
        case 2:
            corner.y +=  r.size.y
            break;
    }

    ellipse(corner.x, corner.y,5)
    return corner
}

function seperating_axis_for_rectangle(axis = new Segment(),r = new Rectangle()){
    var n = subtract_vector(axis.point1,axis.point2)
    var rEdgeA = new Segment()
    rEdgeA.point1 = rectangle_corner(r,0)
    rEdgeA.point2 = rectangle_corner(r,1)
    var rEdgeB = new Segment()
    rEdgeB.point1 = rectangle_corner(r,2)
    rEdgeB.point2 = rectangle_corner(r,3)
    
    var rEdgeARange = project_segement(rEdgeA,n)
    var rEdgeBRange = project_segement(rEdgeB,n)
    var rProjection = range_hull(rEdgeARange,rEdgeBRange)
    axisRange = project_segement(axis,n)
    return !overlapping_ranges(axisRange,rProjection)
}

function oriented_rectangle_corner(r = new OrientedRectangle(),nr){
    c = r.halfExtend
    switch(nr % 4){
        case 0:
            c.x = -c.x
            break;
        case 1:
            //c = r.halfExtend
            break;
        case 2:
            c.y = -c.y
            break;
        default:
            c = negate_vector(c)
            break;
    }
    c = rotate_vector(c,r.rotation)
    return add_vector(c,r.center)
}

function abs_minimum(a,b){
    return a < b ? a : b
}

function abs_maximum(a,b){
    return a > b ? a : b
}

function enlarge_rectangle_point(r = new Rectangle(),p = new Vector2D()){
    var enlargedR = new Rectangle()

    
    enlargedR.origin.x = abs_minimum(r.origin.x,p.x)
    enlargedR.origin.y = abs_minimum(r.origin.y,p.y)
    enlargedR.size.x = abs_maximum(r.origin.x + r.size.x,p.x)
    enlargedR.size.y = abs_maximum(r.origin.y + r.size.y,p.y)
    
    enlargedR.size = subtract_vector(enlargedR.size,enlargedR.origin)
    return enlargedR;
}

function oriented_rectangle_rectangle_hull(r=new OrientedRectangle()){
    var h = new Rectangle({x:r.center.x,y:r.center.y});
    //var corner 

    for(var nr =1;nr < 5;nr++){
        var corner = oriented_rectangle_corner(r,nr)
        h = enlarge_rectangle_point(h,corner)
    }
   
    return h
}

function oriented_rectangle_rectangle_collide(or = new OrientedRectangle(),aar = new Rectangle()){
    var orHull = oriented_rectangle_rectangle_hull(or)
    
    if(!rectangles_collide(orHull,aar))
        return false
    var edge = oriented_rectangle_edge(or,0)
    if(seperating_axis_for_rectangle(edge,aar))
        return false
    edge = oriented_rectangle_edge(or,1)
    return !seperating_axis_for_rectangle(edge,aar)
    //return true
}

function line_point_collide(l = new Line(),p= new Vector2D()){
    if(points_collide(l.base,p))
        return true
    var lp = subtract_vector(p,l.base)
    if(parallel_vectors(lp,l.dir))
        return true
    else
        return false
}

function point_segment_collide(p= new Vector2D(),s = new Segment()){
    var d = subtract_vector(s.point2,s.point1)
    var lp = subtract_vector(p,s.point1)
    var pr = project_vector(lp,d)
    return equal_vectors(lp,pr) &&
        vector_length(pr) <= vector_length(d)&&
        0 <= dot_product(pr,d)
}

function oriented_rectangle_point_collide(r= new OrientedRectangle(),p = new Vector2D()){
    var lr = new Rectangle()
    lr.size = multiply_vector(r.halfExtend,2)
    var lp = subtract_vector(p,r.center)
    lp = rotate_vector(lp,-r.rotation)
    lp = add_vector(lp,r.halfExtend)
    return point_rectangle_collide(lp,lr)
}

function line_segment_collide(l = new Line(),s = new Segment()){
    return !on_one_side(l,s)
}

function line_oriented_rectangle_collide(l= new Line(),r = new OrientedRectangle()){
    var lr = new Rectangle()
    lr.size = multiply_vector(r.halfExtend,2)
    var l1 = new Line()
    l1.base = subtract_vector(l.base,r.center)
    l1.base = rotate_vector(l1.base,-r.rotation)
    l1.base = add_vector(l1.base,r.halfExtend)
    l1.dir = rotate_vector(l.dir,-r.rotation)
   
    return line_rectangle_collide(l1,lr)
}

function oriented_rectangle_segment_collide(r = new OrientedRectangle(),s= new Segment()){
    var lr = new Rectangle()
    lr.origin.x = 0
    lr.origin.y = 0
    lr.size = multiply_vector(r.halfExtend,2)
    var ls = new Segment()
    ls.point1 = subtract_vector(s.point1,r.center)
    ls.point1 = rotate_vector(ls.point1,-r.rotation)
    ls.point1 = add_vector(ls.point1,r.halfExtend)
    
    ls.point2 = subtract_vector(s.point2,r.center)
    ls.point2 = rotate_vector(ls.point2,-r.rotation)
    ls.point2 = add_vector(ls.point2,r.halfExtend)
    
    return rectangle_segment_collide(lr,ls)
}
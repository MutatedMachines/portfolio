

class Line{
    base = {x:0,y:0}
    dir = {x:0,y:0}

    constructor(base = {x:0,y:0},dir = {x:0,y:0}){
        this.base.x = base.x
        this.base.y = base.y
        this.dir.x = dir.x
        this.dir.y = dir.y
    }

    static create(baseX,baseY,dirX,dirY){
        return new Line({x:baseX,y:baseY},{x:dirX,y:dirY})
    }

    draw(){
        line(this.base.x- this.dir.x * 1000 ,
            this.base.y - this.dir.y * 1000,
            this.base.x + this.dir.x * 1000 ,
            this.base.y + this.dir.y * 1000)
    }
}

class Segment{
    point1 = {x:0,y:0}
    point2 = {x:0,y:0}

    constructor(point1 = {x:0,y:0},point2 = {x:0,y:0}){
        this.point1 = point1
        this.point2 = point2
    }

    static create(p1X,p1Y,p2X,p2Y){
        return new Segment({x:p1X,y:p1Y},{x:p2X,y:p2Y})
    }

    draw(){
        line(this.point1.x,this.point1.y,
            this.point2.x,this.point2.y)
    }
}

class Circle{
    center = {x:0,y:0}
    radius = 0

    get x (){ return this.center.x}
    get y (){ return this.center.y}

    constructor(center = {x:0,y:0},radius = 0){
        this.center = center
        this.radius = radius
    }

    static create(x,y,radius){
        return new Circle({x:x,y:y},radius)
    }

    draw(){
        ellipse(this.center.x, this.center.y, this.radius*2)
    }
}

class Rectangle{
    origin = {x:0,y:0}
    size = {x:0,y:0}


    constructor(x,y,w,h){
        this.origin = {x:x,y:y}
        this.size = {x:w,y:h}
    }

    static create(x,y,w,h){
        return new Rectangle(x,y,w,h)  //NOTED:: UNSURE OF CHANGE FROM TUPPLES TO xywh may be older version of code
    }

    draw(){
        console.log(this.origin.x)
        rect(this.origin.x,this.origin.y,this.size.x,this.size.y)
    }
}


class OrientedRectangle{
    center = {x:0,y:0}
    halfExtend = {x:0,y:0}
    rotation = 0

    constructor(center= {x:0,y:0},halfExtend= {x:0,y:0},rotation){
        this.center = center
        this.halfExtend = halfExtend
        this.rotation = rotation
    }

    static create(x,y,w,h,rotation){
        return new OrientedRectangle({x:x,y:y},{x:w/2,y:h/2},rotation)
    }

    draw(){
        push()
        rectMode(CENTER)
        translate(this.center.x,this.center.y)
        rotate(degrees_to_radian(this.rotation))
        rect(0,0,this.halfExtend.x*2,this.halfExtend.y*2)
        pop()
    }
}

class sRange{
    minimum
    maximum

    constructor(min = 0,max = 0){
        this.minimum = min
        this.maximum = max
    }
}

function testDrawShapes(){
    push()
    translate(250, 250)
    scale(10,-10)

    strokeWeight(0.3)

    line(0,0,100,0)
    line(0,0,0,100)

    var p = new Vector2D(3,3)
    var d1 = new Vector2D(7,-2)
    var d2 = new Vector2D(3,5)

    var l1 = new Line(p,d1)
    var l2 = new Line(p,d2)
 //   l1.draw()
   // l2.draw()
    

   var p1 = new Vector2D(3,4)
   var p2 = new Vector2D(11,1)
   var p3 = new Vector2D(8,4)
   var p4 = new Vector2D(11,7)

   var a = new Segment(p1,p2)
   var b = new Segment(p3,p4)

   a.draw()
   b.draw()

    var c = new Vector2D(6,4)
    var r = 4

    var cA = new Circle(c,r)
    cA.draw()


    var origin = new Vector2D(2,3)
    var size = new Vector2D(6,4)
    var r = new Rectangle(origin,size)
    r.draw()

    var center = new Vector2D(6,4)
    var halfExtend = new Vector2D(3,2)
    var rotation = 30

    var or = new OrientedRectangle(center,halfExtend,rotation)
    or.draw()
    pop()

   
}
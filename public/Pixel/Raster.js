

//Octal function
//https://www.includehelp.com/code-snippets/convert-decimal-to-octal-and-vice-versa-in-javascript.aspx

function OctToDec(oct){
    return parseInt(oct,8)
}


//pixel data is sorted first by X then Y then TileX then TileY
//this is to keep tile data together
function ConvertToTileSpace(x,y){
    var oct = 0000
    oct +=  Math.trunc(y/8).toString(8)
    oct +=  Math.trunc(x/8).toString(8)
    oct +=  Math.trunc(y%8).toString(8)
    oct +=   Math.trunc(x%8).toString(8)
    return oct
}






//this is a container for manageing the pixel data
class PixelPage{
    pix = []
    indices = []

    constructor(){
        for(var i =0; i< 8*8*8*8;i++){
            this.pix.push(color('#f0f0'));
        }
        this.indices = new Int8Array(8*8*8*8)
    }

    refresh(){
        for(var i =0; i < this.pix.length;i++){
            if(this.indices[i] != 0)
                this.pix[i] = Pal.Pal[this.indices[i]]
        }
    }

    setindice(x,y,i){
        var oct =  ConvertToTileSpace(x,y)
        this.indices[parseInt(oct,8)] = i
        this.pix[parseInt(oct,8)] = Pal.Pal[i]
    }


    getIndex(x,y){
        var oct =  ConvertToTileSpace(x,y)
        return this.indices[parseInt(oct,8)]
    }
}


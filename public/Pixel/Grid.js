

class Grid{
    _x =1
    _y =1
    _W = 1
    _H = 1


    get x(){return width *this._x;}
    get y(){return height *this._y;}
    set x(X){this._x = X;}
    set y(Y){this._y = Y;}

    get w(){return width *this._W;}
    get h(){return height *this._H;}
    set w(W){this._W = W;}
    set h(H){this._H = H;}
    

    divX
    divY


    constructor(x,y,w,h,rows,columns){
        this.x = x
        this.y = y
        this.w = w
        this.h = h
        this.divX = rows
        this.divY = columns
    }



    Within(x,y,offsetX=0,offsetY=0){
        return Within(x-offsetX,y-offsetY,this.x,this.y,this.w,this.h)
    }
    
    SelectionIndex(offsetX=0,offsetY=0){
        var Pos = {x:mouseX-offsetX,y:mouseY-offsetY}

        if(Within(Pos.x,Pos.y,this.x,this.y,this.w,this.h)){
                var celW = this.w / this.divX
                var celH = this.h / this.divY
                var selX = Math.trunc(((Pos.x - this.x) /celW)%this.divX)
                var selY = Math.trunc(((Pos.y - this.y) /celH)%this.divY)

                
                //console.log(selX)
                return {i:selX + selY * this.divX, x:selX,y:selY}
        }
        return false
    }
    


    Draw(Pal =[]){
        var i =0
        for(var y=0; y < this.divY;y++)
        for(var x=0; x < this.divX;x++)
        {
            if(Pal.length!= 0)
                fill(Pal[i% Pal.length])
            this.DrawCell(x,y)
            i++
        }
    }

    clear(offset = {x:0,y:0}){
        push()
        noStroke()
        fill(Pal.Pal[0])
        rect(this.x+offset.x,this.y+offset.y,this.w,this.h)
        pop()
    }

    DrawRaster(Pal =[],offset = {x:0,y:0}){
        var i =0
        for(var y=0; y < this.divY;y++)
        for(var x=0; x < this.divX;x++)
        {
            var Pixel = Pal[OctToDec(ConvertToTileSpace(x + offset.x*8,y+ offset.y*8)) % Pal.length]

            if(Pixel!==false){
                if(Pal.length!= 0)
                    fill(Pixel)
                this.DrawCell(x,y)
            }
            //textSize(10)
            //this.DrawCellText(x,y,OctToDec(ConvertToTileSpace(x,y)))
            i++
        }
        
    }

    DrawText(txt =[]){
        var i =0
        for(var y=0; y < this.divY;y++)
        for(var x=0; x < this.divX;x++)
        {
          //  if(Pal.length!= 0)
            //    fill(Pal[i% Pal.length])
            this.DrawCellText(x,y,txt[i%txt.length] )
            i++
        }
    }


    DrawRange(posX,posY,W,H){
        for(let y = posY;   y<posY+H;   y++)
            for(let x = posX;   x<posX+W;   x++)
                this.DrawCell(x,y)
    }

    

    DrawCell(Cel_X,Cel_Y){
        var CelWidth = this.w / this.divX
        var CelHeight = this.h / this.divY
        rect(this.x + CelWidth *Cel_X,this.y + CelHeight * Cel_Y,CelWidth,CelHeight)
    }

    DrawCellText(Cel_X,Cel_Y,txt){
        var CelWidth = this.w / this.divX
        var CelHeight = this.h / this.divY
        //rect(this.x + CelWidth *Cel_X,this.y + CelHeight * Cel_Y,CelWidth,CelHeight)
        text(txt, this.x + CelWidth *Cel_X, this.y + CelHeight * Cel_Y, CelWidth, CelHeight)
    }

    DrawSubGrid(divX,divY){
        var i =0
        for(var y=0; y < divY;y++)
        for(var x=0; x < divX;x++)
        {

            var CelWidth = this.w / divX
            var CelHeight = this.h / divY
            rect(this.x + CelWidth *x,this.y + CelHeight * y,CelWidth,CelHeight)
            
            //this.DrawCell(x,y)
            i++
        }
    }

    DrawCellIndex(i){
        i = abs(i % (this.divX * this.divY))
        var Cel_X = i % this.divX
        var Cel_Y = Math.trunc(i /this.divX) % this.divY

        var CelWidth = this.w / this.divX
        var CelHeight = this.h / this.divY
        rect(this.x + CelWidth *Cel_X,this.y + CelHeight * Cel_Y,CelWidth,CelHeight)
    }

}


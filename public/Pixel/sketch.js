

//Global Variables
var Pal 
var RasterPage
var PalSelect =3
var TileOffset = {x:5,y:2}
var TileRange = {x:1,y:1}
var BGCycle = []
var inverseBG 
var complimentBG
var FlagRefresh = true;
var Panels = []
var cvnDiv
var csvPallet 



function setup() {
  //console.clear()
  // create the canvas (1200px wide, 800px high)
  cvnDiv = document.getElementById("myCanvas")
  var cvnwidth = cvnDiv.offsetWidth
  var cvnheight = cvnDiv.offsetHeight
  cvn = createCanvas(cvnwidth, 250)
  color(32,32,256)
  background(32,32,128)
  cvn.parent("myCanvas")
  
  toggleNav() 
  
  windowResized()

  Pal = new Pallet(color(0,0,256,256),color(0,256,0,256),32)
  Pal.loadPallet(vanilla_milkshake,16)
  //Pal.Pal[0] = color(32,32,32)
  RasterPage = new PixelPage()
  
  //surpress browser context menu
  //found here https://stackoverflow.com/questions/60853612/p5-js-on-right-mouse-click-shows-the-browser-context-menu-and-not-the-drawing-fu
  for (let element of document.getElementsByClassName("p5Canvas")) {
    element.addEventListener("contextmenu", (e) => e.preventDefault());
  }

  BGCycle = [color('#511'),color('#461'),color('#151'),color('#145'),color('#114'),color('#514')]


  //most of the functionailty is found in panels these
  //inheret from the same base and functionality is controlled
  Panels = [
    new pPallet(),
    new pPixelGrid(),
    new pTileRaster(),
    new pTileGrid(),
    new pColorSelect()
  ]
}


//trigger the panels on input
function mousePressed() {
  Panels.forEach(p=> p.click())
}

function mouseDown() {
    Panels.forEach(p=> p.held())
}

function mouseReleased(){
    Panels.forEach(p=> p.release())
}



function draw() {
  windowResized()

  
  if(mouseIsPressed)
    mouseDown()


  Panels.forEach(p=> p.Update())


  //this produces the color cycle 
  //if you wish to aceess without it i recomend
  //comenting it out if its straining your eyes
  //BGCycleing()
  FlagRefresh = true


  //this sets the background
  if(FlagRefresh){
    background(Pal.Pal[0])
    complimentBG =  colorShiftNight(compliment(Pal.Pal[0]),0.1/3,0.1/3,0.1/3)
    inverseBG = colorShiftNight(Pal.Pal[0],0.1*0.66,0.1*0.66,0.1*0.66)
    
  }



  //guidelines for spaceing
  //line(width/2, 0, width/2, height) 
  //line(width, 0, width, height)
  //line(0,height/2,width,height/2)
  //line(0,height/4,width,height/4)

  console.log(width);

  Panels.forEach(p=> p.Draw())

  FlagRefresh = false;
}

// when you hit the spacebar, what's currently on the canvas will be saved (as a
// "thumbnail.png" file) to your downloads folder
function keyTyped() {
  if (key === " ") {
    saveCanvas("thumbnail.png");
  }
}

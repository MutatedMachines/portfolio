---
# for information on what this file is, see
# https://cs.anu.edu.au/courses/comp1720/resources/faq/#statement-of-originality

# for styling of references, see
# https://www.acm.org/publications/authors/reference-formatting

declaration: >-
  I declare that everything I have submitted in this assignment is entirely my
  own work except for the references listed below

# sign *your* name and uid here
name: Josh Carberry
uid: u4386672

# list numbered references below (replace the example with your own references) 
---
# References
- [1] Example Author. 2022. p5.js example code video. Retrieved from https://example.com/video

- [2] @VGDensetsu https://vgdensetsu.tumblr.com/post/179656817318/designing-2d-graphics-in-the-japanese-industry

similar works
-[3] Monster 6502 Eric Schlaepfer 2016 https://monster6502.com/  
the monster 6502 is a functioning scale replica of the Mos6502 micro controller used in many modern microcomputers. This work is used to give the veiwer a sence of how micro computers work by illuming active regiters using LEDs as well as to explore the minute scale of micro processers by expanding the surface volume and using discrete logical components Eric displays his technical understanding of the subject and teaches about the historical importance of the 6502
Monster 6502 was first displayed at the 2016 Bay Area Maker Faire. 

-[4] artstation profile of Rachid Lotf https://www.artstation.com/rachidlotf
Rachid Loft explores nostalgia in his paintings of fictional bedrooms and idealizing the consumer culture he explores the expeince of growing up in the 1980s and explores how cultural objects gain meaning thru their use in our everyday life.
The objects in his works are ones of modern consumerizimn but by applying clasical art techneqes he elevates their importance and makes an argument against dismising modern historical experiences.

artistic validation
-[5] Five creatives inspired by early gaming nostalgia. Liz Gorny 20 July 2022 https://www.itsnicethat.com/articles/five-creatives-vintage-gaming-illustration-200722
-[6] Article exploreing modern nostagia in art Lyle burns may 17 2019
https://medium.com/@noisecomplaintsgroup/artists-are-tapping-into-nostalgia-because-it-works-e6ef6e6c5426 

prior work
-[7] MutantMecha July 6 2021  https://twitter.com/MutantMecha/status/1412252753468944391
Last year i attempted to create a similar pixel editor in unity. I was attempting to create a panel for unity to be sold on their store. It wasn't intended as a work of art but was meant for enabling art. this was also inspired by the VGDensetsu article. None of the code from this was reused within this project and i can provide what still exists of the source code if necisary.

# code reference
-[8]surpress browser context menu found here https://stackoverflow.com/questions/60853612/p5-js-on-right-mouse-click-shows-the-browser-context-menu-and-not-the-drawing-fu
-[9]Octal function https://www.includehelp.com/code-snippets/convert-decimal-to-octal-and-vice-versa-in-javascript.aspx
-[10]function to invert a color https://editor.p5js.org/awdriggs/sketches/KMOpsQfDz
-[11]hue shift color theory https://www.youtube.com/watch?v=PNtMAxYaGyg
  
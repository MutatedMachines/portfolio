
//panels inheret from here
class Panel{
    x
    y

    width
    height
    
    row
    columns

    grid

    
    awaitRefresh = false


    //these store cell positions used in logic
    clickedCell = {i:false,x:false,y:false}
    heldCell = {i:false,x:false,y:false}
    lastHeld = {i:false,x:false,y:false}
    released = {i:false,x:false,y:false}
    lastreleased = {i:false,x:false,y:false}
    hovered = {i:false,x:false,y:false}

    //overload functions
    onClick(){}
    onHover(){}
    onHeld(){}
    onRelease(){}
    DrawPanel(){}


    constructor(){
      if(this.grid == null)
        this.grid = this.getGrid()
    }

    setPanel(x=null,y=null,width=null,height=null,rows=null,columns=null){
      if(x != null)
        this.x = x
      if(y != null)
        this.y = y
      if(width != null)
        this.width = width
      if(height != null)
        this.height = height
      if(rows != null)
        this.rows = rows
      if(columns != null)
        this.columns = columns
      
      this.grid = this.getGrid()
      return this
    }


    getGrid(){
    //  if(this.grid == null)
        return new Grid(this.x,this.y,this.width,
          this.height,this.columns,this.row)
    }

    Update(){
      if(this.grid.Within(mouseX,mouseY,this.x,this.y))
        this.hover()
      else
        this.hovered = {i:false,x:false,y:false}
    }

    click(){
        
        var sel =this.grid.SelectionIndex(this.x,this.y)
        
        if(sel!==false){
          this.clickedCell = sel
          this.onClick()
        }
    }

    hover(){
      var sel =this.grid.SelectionIndex(this.x,this.y)
        
        if(sel!==false){
         // this.onHeld()
          this.hovered = sel
          this.onHover()
        }      

    }

    held(){
        var sel =this.grid.SelectionIndex(this.x,this.y)
        
        if(this.heldCell.i!==false)
          this.lastHeld = this.heldCell
       // console.log("held")

        if(sel!==false){
          this.heldCell = sel
          this.onHeld()
        }
    }

    release(){
      var sel =this.grid.SelectionIndex(this.x,this.y)
        
      if(this.released.i!==false)
        this.lastreleased = this.released
      // console.log("held")

      if(sel!==false){
        this.released = sel
        this.onRelease()
      }
    }

    Draw(){
      if(this.awaitRefresh===false || (FlagRefresh == this.awaitRefresh)){
        push()
        translate(this.x, this.y)
        if(this.grid!=null)
            this.DrawPanel()
        pop()
      }
    }

}

//color select panel
class pColorSelect extends Panel{
  x=0.05
  y=0.05

  width = 0.9
  height = 0.2

  row = 16
  columns = 3

  fStops = 16

  pallet 

  grid = this.getGrid()

  H
  S
  L
  
  awaitRefresh = true

  constructor(){
    super()
    this.grid.divX = this.fStops
    this.grid.divY = 3
    this.pallet = new Pallet(color('#0000'),color('#ffff'),this.fStops*3)
  }

  onRelease(){
    Pal.setColor(PalSelect, this.pallet.Pal[this.released.i])
    RasterPage.refresh()
    FlagRefresh = true
  }

  DrawPanel(){
    this.H = hue(Pal.Pal[PalSelect])
    this.S = saturation(Pal.Pal[PalSelect])
    this.L = lightness(Pal.Pal[PalSelect])

    
    this.pallet.HSLRange(this.H,this.S,this.L)

    noStroke()
    //strokeWeight(3)
    //stroke(inverseBG)
    this.grid.Draw(this.pallet.Pal)

    
    strokeWeight(3)
    stroke(inverseBG)
    noFill()
    this.grid.DrawSubGrid(2,1)


    stroke(complimentBG)
    noFill()
    this.grid.DrawCellIndex(this.heldCell.i)
  }

}

//controlls the pixel grid to the right
class pPixelGrid extends Panel{
  x = 0.6 ;
  y = 0.3

  width = 0.65 * (height/width)
  height = 0.65 
  columns = 16
  row = 16

  grid = this.getGrid()

  //awaitRefresh = false

  onClick(){
    
  }

  onHeld(){    
    if(mouseButton == LEFT)
      if(this.lastHeld.i != this.heldCell.i){
        RasterPage.setindice(this.heldCell.x + TileOffset.x*8,this.heldCell.y+TileOffset.y*8,PalSelect)
        FlagRefresh = true
      }

    if(mouseButton == RIGHT)
      if(this.lastHeld.i != this.heldCell.i){
        RasterPage.setindice(this.heldCell.x + TileOffset.x*8,this.heldCell.y+TileOffset.y*8,0)
        //var rasI = RasterPage.getIndex(this.heldCell.x + TileOffset.x*8,this.heldCell.y+TileOffset.y*8,0)
        //RasterPage.setColor(rasI,color('#0000'))
        FlagRefresh = true
      }
  }

  DrawPanel(){
    this.grid.clear()

    if(this.grid.divX != TileRange.x*8){
      this.grid.divX = TileRange.x*8
      this.grid.divY = TileRange.y*8
    }
    
    strokeWeight(2/TileRange.x)
    fill(complimentBG)
    stroke(inverseBG)    
    this.grid.DrawRaster(RasterPage.pix,TileOffset)


    noFill()
    strokeWeight(2)
    stroke(complimentBG)
    this.grid.DrawSubGrid(TileRange.x,TileRange.y)
    
  }
}

//this is for drawing the image under the tile grid
//just simpler to treat as seperate object
class pTileRaster extends Panel{
  x = 0.05 ;
  y = 0.3

  width = 0.65 * (height/width)
  height = 0.65
  
  columns = 64
  row = 64

  grid = this.getGrid()

  awaitRefresh = true
  //grid = new Grid(0,0,512,512,64,64)

  DrawPanel(){
    noStroke()
    this.grid.DrawRaster(RasterPage.pix)
  }
}

//tile grid for selecting the editable range
class pTileGrid extends Panel{
    x = 0.05 ;
    y = 0.3

    width = 0.65 * (height/width)
    height = 0.65 

    row = 8
    columns = 8

    grid = this.getGrid()

    awaitRefresh = true

    onHeld(){
      if(mouseButton == LEFT)
      {
        var xRange = getRange([this.clickedCell.x,this.heldCell.x])
        var yRange = getRange([this.clickedCell.y,this.heldCell.y])
        
        var xSize = xRange.max - xRange.min
        var ySize = yRange.max - yRange.min

        var sizeRange = getRange([xSize+1,ySize+1])

        TileOffset.x = xRange.min
        TileOffset.y = yRange.min

        if(TileRange.x != sizeRange){
          TileRange.x = sizeRange.max
          TileRange.y = sizeRange.max
          
          FlagRefresh = true
        }

      }
    }
    
    DrawPanel(){
        noFill()
        strokeWeight(2)
        stroke(inverseBG,128)
        this.grid.Draw()
         
        strokeWeight(3)
        stroke(complimentBG)        
        this.grid.DrawRange(TileOffset.x,TileOffset.y,TileRange.x,TileRange.y)
    }
}

//this is a pallet panel
//for most of this project i had pallet controll
//I removed it for useability reasons but still beleive this is 
//the way to go if this were to be a serious dev tool
class pPallet extends Panel{
    x = 0.45 ;
    y = 0.3

    width = 0.1
    height = 0.65


    row = 8
    columns = 2

    grid = this.getGrid()

    onHeld(){
      if(mouseButton == LEFT)
        PalSelect =this.heldCell.i
    }

    DrawPanel(){
        
        strokeWeight(2)
        stroke(inverseBG)
        this.grid.Draw(Pal.Pal)
        
        noFill()
        stroke(complimentBG)
        if(this.clickedCell!=null)
        this.grid.DrawCellIndex(PalSelect)
      }
}


//I removed tools for scopeing reasons
class pToolBox extends Panel{
    x = 540+128
    y =32

    width = 172
    height = 172
    row = 4
    columns = 2

    grid = this.getGrid()

    DrawPanel(){
        //var ToolBox = new Grid(width - 256,0,172,172,2,4)
        var ToolsText = ['fill','dot','line','sample']
        noFill()
        stroke('#fff3')
        strokeWeight(2)
        this.grid.Draw()
      
        textSize()
        textAlign(CENTER, CENTER)
        textSize(20)
        this.grid.DrawText(ToolsText)
        
        stroke(complimentBG)
        this.grid.DrawCellIndex(this.clickedCell.i)
    }
}
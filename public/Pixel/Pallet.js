var vanilla_milkshake = [
'#777777','#6c5671','#d9c8bf','#f98284',
'#b0a9e4','#accce4','#b3e3da','#feaae4',
'#87a889','#b0eb93','#e9f59d','#ffe6c6',
'#dea38b','#ffc384','#fff7a0','#fff7e4',
]

function Within(x,y,BX,BY,BW,BH){
    if(x > BX && x < BX + BW && y> BY && y < BY+BH )
        return true
    else
        return false
}

function sortRange(a,b){
    r = {min:a,max:a}

    if(a<b){
        r.min = a
        r.max = b
    }
    else
    {
        r.min = b
        r.max = a
    }
    return r
}

//function to invert a color
//https://editor.p5js.org/awdriggs/sketches/KMOpsQfDz
function invert(c,a = 128){
    push()
    colorMode(RGB)
    let inR = 255 - red(c);
    let inG = 255 - green(c);
    let inB = 255 - blue(c);
    
   
  
    //return inR, inG, inB
    return color(inR, inG, inB,a);
    pop()
  }
  
  
  var BGindex =0
  function BGCycleing(){
    push()
    colorMode(HSL)
    BGindex+= 1/1000 
    BGindex = BGindex % BGCycle.length 
    
    Pal.Pal[0] = lerpColor(
      BGCycle[floor(BGindex) % BGCycle.length],
      BGCycle[ceil(BGindex) % BGCycle.length],
      BGindex%1)
    pop()
    FlagRefresh = true
  }
  

  //hue shift color theory
  //https://www.youtube.com/watch?v=PNtMAxYaGyg
  
  function colorShiftNight(c,HS,SS,BS,a = 256){
    push()
    colorMode(HSB)
    let inH = hue(lerpColor(c, color('#84ff'), HS));
  
    let inS = lerp(saturation(c),1,SS)
  
    let inB = lerp(saturation(c),0,BS)
  
    //return inR, inG, inB
    return color(inH, inS, inB,a);
  pop()
  }
  
  function compliment(c,a = 256){
    push()
    colorMode(HSB)
    let inH = ( hue(c)+180) %360;
    
    let inS = saturation(c) ;
    //let inS = 100-saturation(c) %100 ;
    let inB = brightness(c);
    //let inB = 100-brightness(c) %100;
    
    //return inR, inG, inB
    return color(inH, inS, inB,a);
    pop()
  }

function getRange(arr =[]){
    if(arr.length == 0)
        return sortRange(0,0)
    
        let r = {min:arr[0],max:arr[0]}

    for(let i =0; i< arr.length;i++){
        r.min = min(r.min,arr[i])
        r.max = max(r.max,arr[i])
    }

    return sortRange(r.min,r.max)    
}

class Pallet{
    Pal = []
    constructor(colorA,colorB,siz){
        for(var i=0; i< siz;i++){
            this.Pal.push(lerpColor(colorA, colorB, (1.0/siz) * i))
        }
    }

    loadPallet(pal,size){
        for(var i =0; i< size;i++){
            this.setColor(i,color(pal[i]))
        }
    }

    setColor(i,inColor){
        colorMode(RGB)
        if(this.Pal.length > i){
            //console.log(inColor)
            //console.log(inColor.toString('#rgba'))
            this.Pal[i] = color(inColor.toString('#rgba'))
        }
    }

    HSLRange(H,S,L){
        push()
        //console.log("here")
       
        var A = 100
        colorMode(HSL)
        //var B = brightness(inColor)
        //if(this.Pal.length >=16){

        var Range = this.Pal.length /3
        var from = 0
        var to = Range
        for(var i = 0;i < 16; i++){
            this.Pal[i] = color((360/Range)*(i),S,L,A)
        }
        from = to
        to += Range
        for(var i = 0;i < 16; i++){
            this.Pal[i+from] = color(H,(100/Range)*(i),L,A)
        }
        from = to
        to += Range
        for(var i = 0;i < 16; i++){
            this.Pal[i+from] = color(H,S,(100/Range)*(i),A)
        }
            
        //}
        pop()
    }
}


